//
//  APINetwork.swift
//  Keysoc
//
//  Created by Daniel Tang on 28/8/2021.
//

import Foundation
import Alamofire

class APINetwork: NSObject {
    static let shared: APINetwork = {
        return APINetwork()
    }()
    
    func apiCall(url: String, method: HTTPMethod, parameters: [String: Any]? , successreturn: @escaping (_ successData: AnyObject?) -> Void, failereturn: @escaping (_ faileData: AnyObject?) -> Void) {
        AF.request(url, method: method, parameters: parameters).response { (response) in
            if let data = response.data {
                successreturn(data as AnyObject)
            }else{
                failereturn(nil)
            }
        }
    }
    
    func getAlbumData(parameters: [String: Any]?, successreturn: @escaping (_ successData: AlbumModel?) -> Void,
                      failereturn: @escaping (_ faileData: AnyObject?) -> Void) {
        APINetwork.shared.apiCall(url: "https://itunes.apple.com/search?term=jack+johnson&entity=album", method: HTTPMethod.get, parameters: parameters, successreturn: { (success) in
            do {
                let json = try JSONSerialization.jsonObject(with: success as! Data, options: []) as? [String: Any]
                let albumModel: AlbumModel = AlbumModel.init(json!)!
                albumModel.results = AlbumContentModel.modelsFromDictionaryArray(json?["results"] as! NSArray)
                successreturn(albumModel)
            } catch {
                failereturn(nil)
            }
        }, failereturn: { (fail) in
            failereturn(nil)
        })
    }
}
