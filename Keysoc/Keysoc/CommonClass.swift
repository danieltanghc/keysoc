//
//  CommonClass.swift
//  Keysoc
//
//  Created by Daniel Tang on 30/8/2021.
//

import UIKit

class CommonClass: NSObject {
    
    func convertDateToString(str: String) -> String {
        let formatter = ISO8601DateFormatter()
        let date = formatter.date(from: str)!
        
        let dateFormatter = DateFormatter()
        // Set Date Format
        dateFormatter.dateFormat = "dd/MM/yyyy"
        // Convert Date to String
        return dateFormatter.string(from: date)
    }
    
    func convertURLToImage(imageUrlString: String) -> UIImage {
        var image: UIImage?
        let imageUrl:URL = URL(string: imageUrlString)!
        let imageData = try? Data(contentsOf: imageUrl)
        image = UIImage(data: imageData!)!
        
        return image!
    }
    
    func loadStoryboard(from storyboard: String, identifier: String) -> UIViewController {
        let uiStoryboard = UIStoryboard(name: storyboard, bundle: nil)
        return uiStoryboard.instantiateViewController(withIdentifier: identifier)
    }
    
    func getFavouriteAlbum() -> [String] {
        var favouritedAlbumCode: [String] = []
        let userDefaults = UserDefaults.standard
        favouritedAlbumCode = userDefaults.stringArray(forKey: "FavouriteAlbum") ?? []
        return favouritedAlbumCode
    }
    
    func updateFavouriteAlbum(code: String, isSave: Bool) {
        let userDefaults = UserDefaults.standard
        var favouritedAlbumCode: [String] = CommonClass().getFavouriteAlbum()
        if isSave {
            favouritedAlbumCode.append(code)
        }else{
            for (index ,albumCode) in favouritedAlbumCode.enumerated() {
                if code == albumCode {
                    favouritedAlbumCode.remove(at: index)
                }
            }
        }
        do {
            try userDefaults.set(favouritedAlbumCode, forKey: "FavouriteAlbum")
        } catch {
            print("can't saveFavouriteAlbum")
        }
    }
    
    func warningAlbert(title: String, message: String, button: String, buttonAction: @escaping ()  -> Void) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: button, style: .default, handler: { action in
            switch action.style{
            case .default:
                print("default")
                buttonAction()
                
            case .cancel:
                print("cancel")
                
            case .destructive:
                print("destructive")
                
            }
        }))
        let rootViewController = UIApplication.shared.keyWindow?.rootViewController
        rootViewController?.present(alert, animated: true, completion: nil)
    }
}
