//
//  GenericTableViewController.swift
//  Keysoc
//
//  Created by Daniel Tang on 29/8/2021.
//

import UIKit

class GenericTableViewController: UITableViewController {
    @IBOutlet var tbl_content: UITableView!
    var headerContent = [GenericContentModel]()
    var tableContent = [GenericContentModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tbl_content.delegate = self
        self.tbl_content.separatorStyle = UITableViewCell.SeparatorStyle.none
        self.tbl_content.rowHeight = UITableView.automaticDimension
        self.tbl_content.sectionHeaderHeight = UITableView.automaticDimension
        self.tbl_content.estimatedRowHeight = 44
        self.tbl_content.estimatedSectionHeaderHeight = 44
        self.tbl_content.backgroundColor = .white
        self.tbl_content.allowsSelection = false
        
        //Register TableViewCell
        self.tbl_content.register(UINib(nibName: "AlbumTableViewCell", bundle: nil), forCellReuseIdentifier: "AlbumTableViewCell")
        self.tbl_content.register(UINib(nibName: "CountTableViewCell", bundle: nil), forCellReuseIdentifier: "CountTableViewCell")
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return self.headerContent.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.tableContent.count
    }
    
    override func tableView(_ tableView: UITableView, estimatedHeightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let header = self.headerContent[section]
        
        if header.contentType == .numberCount {
            let headerView = tableView.dequeueReusableCell(withIdentifier: "CountTableViewCell") as! CountTableViewCell
            headerView.setupUI(obj: header)
            return headerView
        }
        
        return UIView()
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let content = self.tableContent[indexPath.row]
        
        if content.contentType == .albumContent {
            let cell = tableView.dequeueReusableCell(withIdentifier: "AlbumTableViewCell") as! AlbumTableViewCell
            cell.setupUI(obj: content)
            return cell
        }
        
        return UITableViewCell()
    }
}
