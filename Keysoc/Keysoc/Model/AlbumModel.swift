//
//  AlbumModel.swift
//  Keysoc
//
//  Created by Daniel Tang on 28/8/2021.
//

import UIKit

public class AlbumModel: NSObject {
    public var resultCount: Int = 0
    public var results: [AlbumContentModel] = []
    
    required public init?(_ dictionary: [String:Any]) {
        self.resultCount = dictionary["resultCount"] as? Int ?? 0
        self.results = dictionary["results"] as? [AlbumContentModel] ?? []
    }
    
    public class func modelsFromDictionaryArray(_ array:NSArray) -> [AlbumModel]
    {
        var models:[AlbumModel] = []
        for item in array
        {
            models.append(AlbumModel((item as! NSDictionary) as! [String : Any])!)
        }
        return models
    }
}

public class AlbumContentModel {
    public var wrapperType: String = ""
    public var collectionType: String = ""
    public var artistId : Int = 0
    public var collectionId: Int = 0
    public var amgArtistId: Int = 0
    public var artistName: String = ""
    public var collectionName: String = ""
    public var collectionCensoredName: String = ""
    public var artistViewURL: String = ""
    public var collectionViewURL: String = ""
    public var artworkUrl60: String = ""
    public var artworkUrl100: String = ""
    public var collectionPrice: CGFloat = 0.0
    public var collectionExplicitness: String = ""
    public var trackCount: Int = 0
    public var copyright: String = ""
    public var country: String = ""
    public var currency: String = ""
    public var releaseDate: String = ""
    public var primaryGenreName: String = ""
    public var contentAdvisoryRating: String = ""
    
    required public init?(_ dictionary: [String:Any]) {
        self.wrapperType = dictionary["wrapperType"] as? String ?? ""
        self.collectionType = dictionary["collectionType"] as? String ?? ""
        self.artistId = dictionary["artistId"] as? Int ?? 0
        self.collectionId = dictionary["collectionId"] as? Int ?? 0
        self.amgArtistId = dictionary["amgArtistId"] as? Int ?? 0
        self.artistName = dictionary["artistName"] as? String ?? ""
        self.collectionName = dictionary["collectionName"] as? String ?? ""
        self.collectionCensoredName = dictionary["collectionCensoredName"] as? String ?? ""
        self.artistViewURL = dictionary["artistViewURL"] as? String ?? ""
        self.collectionViewURL = dictionary["collectionViewURL"] as? String ?? ""
        self.artworkUrl60 = dictionary["artworkUrl60"] as? String ?? ""
        self.artworkUrl100 = dictionary["artworkUrl100"] as? String ?? ""
        self.collectionPrice = dictionary["collectionPrice"] as? CGFloat ?? 0.0
        self.collectionExplicitness = dictionary["collectionExplicitness"] as? String ?? ""
        self.trackCount = dictionary["trackCount"] as? Int ?? 0
        self.copyright = dictionary["copyright"] as? String ?? ""
        self.country = dictionary["country"] as? String ?? ""
        self.currency = dictionary["currency"] as? String ?? ""
        self.releaseDate = dictionary["releaseDate"] as? String ?? ""
        self.primaryGenreName = dictionary["primaryGenreName"] as? String ?? ""
        self.contentAdvisoryRating = dictionary["contentAdvisoryRating"] as? String ?? ""
    }
    
    public class func modelsFromDictionaryArray(_ array:NSArray) -> [AlbumContentModel]
    {
        var models:[AlbumContentModel] = []
        for item in array
        {
            models.append(AlbumContentModel((item as! NSDictionary) as! [String : Any])!)
        }
        return models
    }
}
