//
//  GenericContentModel.swift
//  Keysoc
//
//  Created by Daniel Tang on 29/8/2021.
//

import UIKit

enum TableViewCellType {
    case albumContent
    case numberCount
}

public class GenericContentModel: NSObject {
    var contentType: TableViewCellType = .numberCount
    var albumContent: AlbumContentModel?
    var titleText: String = ""
    var contentImage: String = ""
    var subTitleText: String = ""
    var contentText: String = ""
    var dateText: String = ""
    var priceText: String = ""
    var remindText: String = ""
    var buttonHandler: ((UIButton) -> Void)? = nil
}

extension GenericContentModel{
    func contentType(_ contentType: TableViewCellType) -> GenericContentModel {
        self.contentType = contentType
        return self
    }
    
    func albumContent(_ albumContent: AlbumContentModel) -> GenericContentModel {
        self.albumContent = albumContent
        return self
    }
    
    func titleText(_ titleText: String) -> GenericContentModel {
        self.titleText = titleText
        return self
    }
    
    func contentImage(_ contentImage: String) -> GenericContentModel {
        self.contentImage = contentImage
        return self
    }
    
    func subTitleText(_ subTitleText: String) -> GenericContentModel {
        self.subTitleText = subTitleText
        return self
    }
    
    func contentText(_ contentText: String) -> GenericContentModel {
        self.contentText = contentText
        return self
    }
    
    func dateText(_ dateText: String) -> GenericContentModel {
        self.dateText = dateText
        return self
    }
    
    func priceText(_ priceText: String) -> GenericContentModel {
        self.priceText = priceText
        return self
    }
    
    func remindText(_ remindText: String) -> GenericContentModel {
        self.remindText = remindText
        return self
    }
    
    func buttonHandler(_ buttonHandler:@escaping ((UIButton) -> Void)) -> GenericContentModel {
        self.buttonHandler = buttonHandler
        return self
    }
}
