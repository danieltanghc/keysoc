//
//  AlbumTableViewCell.swift
//  Keysoc
//
//  Created by Daniel Tang on 29/8/2021.
//

import UIKit

class AlbumTableViewCell: UITableViewCell {
    @IBOutlet var iv_albumtThumbnail: UIImageView!
    @IBOutlet var lbl_albumName: UILabel!
    @IBOutlet var lbl_genre: UILabel!
    @IBOutlet var lbl_artistName: UILabel!
    @IBOutlet var lbl_releaseDate: UILabel!
    @IBOutlet var lbl_price: UILabel!
    @IBOutlet var iv_heart: UIImageView!
    @IBOutlet var btn_heart: UIButton!
    var isFavAlbum: Bool = false
    var genericContentModel: GenericContentModel?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupUI(obj: GenericContentModel) {
        let favouriteAlbum: [String] = CommonClass().getFavouriteAlbum()
        
        if let albumContent: AlbumContentModel = obj.albumContent {
            self.genericContentModel = obj
            let title: String = albumContent.collectionCensoredName + " (" + String(albumContent.trackCount) + ")"
            let price: String = albumContent.currency + (albumContent.collectionPrice.description)
            let date: String = "Release Date: " + CommonClass().convertDateToString(str: albumContent.releaseDate)
            
            self.lbl_albumName.text = title
            self.lbl_albumName.font = UIFont(name: "HelveticaNeue-Bold", size: 14)
            self.lbl_albumName.numberOfLines = 3
            
            self.lbl_genre.text = albumContent.primaryGenreName
            self.lbl_genre.font = UIFont(name: "HelveticaNeue-Thin", size: 9)
            self.lbl_genre.textAlignment = .center
            
            self.lbl_artistName.text = albumContent.artistName
            self.lbl_artistName.font = UIFont(name: "HelveticaNeue", size: 12)
            
            self.lbl_releaseDate.text = date
            self.lbl_releaseDate.font = UIFont(name: "HelveticaNeue", size: 12)
            
            self.lbl_price.text = price
            self.lbl_price.font = UIFont(name: "HelveticaNeue", size: 12)
            
            if favouriteAlbum.count > 0 {
                isFavAlbum = favouriteAlbum.contains(String(albumContent.collectionId))
            }
            self.iv_heart.image = UIImage(named: isFavAlbum ? "heart_On.png" : "heart_Off.png")
            do {
                let image: UIImage = try (CommonClass().convertURLToImage(imageUrlString: obj.albumContent!.artworkUrl100))
                self.iv_albumtThumbnail.image = image
            } catch {
                self.iv_albumtThumbnail.image = UIImage(named: "default_album.png")
            }
            
            self.layoutIfNeeded()
        }
    }
    
    @IBAction func clickHeartButtonAction(_ sender: UIButton) {
        CommonClass().updateFavouriteAlbum(code: String(self.genericContentModel!.albumContent!.collectionId), isSave: !self.isFavAlbum)
        if self.genericContentModel?.buttonHandler != nil {
            self.genericContentModel?.buttonHandler!(sender)
        }
        UIView.animate(
            withDuration: 0.5,
            animations: {
                self.iv_heart.image = UIImage(named: self.isFavAlbum ? "heart_Off.png" : "heart_On.png")
            })
        
    }
}
