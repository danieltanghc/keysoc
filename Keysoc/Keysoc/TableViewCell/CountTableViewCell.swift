//
//  CountTableViewCell.swift
//  Keysoc
//
//  Created by Daniel Tang on 29/8/2021.
//

import UIKit

class CountTableViewCell: UITableViewCell {
    
    @IBOutlet var lbl_count: UILabel!
    @IBOutlet var vw_content: UIView!
    @IBOutlet var layout_vwContentWidth: NSLayoutConstraint!
    
    func setupUI(obj: GenericContentModel) {
        self.lbl_count.text = obj.titleText + " Album Count(s)"
        self.lbl_count.font = UIFont(name: "HelveticaNeue", size: 17)
        self.lbl_count.textAlignment = .center
        self.lbl_count.numberOfLines = 1
        
        self.layout_vwContentWidth.constant = self.lbl_count.frame.width
        self.vw_content.backgroundColor = .lightGray
        self.vw_content.layer.cornerRadius = 5
    }
}
