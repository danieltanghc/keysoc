//
//  ViewController.swift
//  Keysoc
//
//  Created by Daniel Tang on 28/8/2021.
//

import UIKit

class ViewController: UIViewController {
    @IBOutlet var nav_item: UINavigationItem!
    var tbl_content: GenericTableViewController!
    var albumContent: AlbumModel?
    var isFavPage: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.updateNavFrame()
        self.setupUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if !self.isFavPage {
            self.getAPIContent()
        }else{
            self.setupContent()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

extension ViewController{
    func updateNavFrame() {
        self.navigationController?.navigationBar.topItem?.title  = "Album List"
        self.navigationController?.navigationBar.backgroundColor = .systemGray
        self.navigationController?.navigationBar.tintColor = .black
        
        let btn_fav = UIButton(type: .custom)
        btn_fav.setImage(UIImage(named: "heart_Off.png"), for: .normal)
        btn_fav.frame = CGRect(x: 0, y: 0, width: 12, height: 12)
        btn_fav.imageView?.contentMode = .scaleAspectFit
        btn_fav.addTarget(self, action: #selector(goToFavPage), for: .touchUpInside)
        
        let item = UIBarButtonItem(customView: btn_fav)
        let currWidth = item.customView?.widthAnchor.constraint(equalToConstant: 24)
        currWidth?.isActive = true
        let currHeight = item.customView?.heightAnchor.constraint(equalToConstant: 24)
        currHeight?.isActive = true
        if !self.isFavPage {
            self.navigationItem.setRightBarButtonItems([item], animated: true)
        }
    }
    
    func getAPIContent() {
        APINetwork.shared.getAlbumData(parameters: nil) { (data) in
            self.albumContent = data
            self.setupContent()
        } failereturn: { (fail) in
            
        }
    }
    
    func setupUI(){
        //Table UI
        self.tbl_content = CommonClass().loadStoryboard(from: "Main", identifier: "GenericTableViewController") as? GenericTableViewController
        addChild(tbl_content)
        self.view.addSubview(self.tbl_content.view)
        self.tbl_content.didMove(toParent: self)
        self.tbl_content.view.frame = CGRect.init(x: 0, y: (self.navigationController?.navigationBar.frame.height)! + 50, width: self.view.frame.width, height: self.view.frame.height)
    }
    
    func setupContent() {
        if self.albumContent != nil {
            let favAlbum: [String] = CommonClass().getFavouriteAlbum()
            var resultCount: Int = 0
            
            self.tbl_content.headerContent = []
            self.tbl_content.tableContent = []
            
            if self.isFavPage {
                resultCount = favAlbum.count
            }else if self.albumContent?.resultCount != nil{
                resultCount = self.albumContent!.resultCount
            }
            
            self.tbl_content.headerContent = [GenericContentModel().contentType(.numberCount).titleText(String(resultCount))]
            
            if self.albumContent?.results != nil &&  (self.albumContent?.results.count)! > 0 {
                for album in self.albumContent?.results ?? [] {
                    if self.isFavPage {
                        if favAlbum.contains(String(album.collectionId)) {
                            self.tbl_content.tableContent.append(GenericContentModel().contentType(.albumContent).albumContent(album).buttonHandler({ (sender) in
                                if favAlbum.count == 1 {
                                    self.navigationController?.popViewController(animated: true)
                                }else{
                                    self.setupContent()
                                }
                            }))
                        }
                    }else{
                        self.tbl_content.tableContent.append(GenericContentModel().contentType(.albumContent).albumContent(album))
                    }
                }
            }
        }
        self.tbl_content.tableView.reloadData()
    }
    
    @objc func goToFavPage() {
        let favAlbum: [String] = CommonClass().getFavouriteAlbum()
        let vcPage = ViewController()
        vcPage.isFavPage = true
        vcPage.albumContent = self.albumContent
        
        if favAlbum.count > 0 {
            self.navigationController?.pushViewController(vcPage, animated: true)
        }else{
            CommonClass().warningAlbert(title: "Havn't Album in Your Fav List", message: "", button: "Ok") {
                
            }
        }
    }
}

